# -*- coding: utf-8 -*-
import re
import json
import urllib.request
import requests

from urllib import parse

from bs4 import BeautifulSoup
from flask import Flask, request
from slack import WebClient

from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter

from threading import Thread

SLACK_TOKEN = ?
SLACK_SIGNING_SECRET = ?

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현
def _crawl_cvs(text, button):
    if len(text.split()) < 2: # @표니봇만 입력한 경우
        result = []
        result.append(SectionBlock(
            text="`@펴니봇 제품명`과 같이 멘션해주세요.",
        ))
        return result

    quote_url = ""
    search_text = ""
    for t in range(1, len(text.split())):
        quote_url += ("+" + parse.quote(text.split()[t]))
        search_text += (" " + text.split()[t])
    if button == " ": # 전체 상품 조회 url
        url = "https://pyony.com/search/?event_type=&category=&item=10&sort=&q=" + quote_url
    else: # 행사 상품 조회 url
        url = "https://pyony.com/search/?event_type=" + button + "&category=&item=10&sort=&q=" + quote_url
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    products = [] # 상품 목록 저장 리스트

    for col_tag in soup.find_all("div", class_="col-md-6"):
        product = {} # 조회한 상품 딕셔너리
        product_info = col_tag.get_text().split()
        product['cvs'] = product_info[0]
        product['name'] = product_info[2]
        product['price'] = product_info[3]
        product['event'] = product_info[6]
        if product['cvs'].startswith("C"): # CU
            product['image'] = "http://cu.bgfretail.com/images/facebook.jpg"
        elif product['cvs'].startswith("G"): # GS25
            product['image'] = "https://is5-ssl.mzstatic.com/image/thumb/Purple123/v4/e2/85/d8/e285d8c8-7c88-868d-af23-bb6c56281d55/AppIcon-0-1x_U007emarketing-0-85-220-4.png/246x0w.jpg"
        elif product['cvs'].startswith("M"): # 미니스톱
            product['image'] = "https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/9f/02/2b/9f022b0f-e0f0-799a-a158-aacbccac8d9d/source/512x512bb.jpg"
        elif product['cvs'].startswith("7"): # 7-ELEVEN
            product['image'] = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/7-eleven_logo.svg/200px-7-eleven_logo.svg.png"
        elif product['cvs'].startswith("E"): # 이마트24
            product['image'] = "http://www.emart24.co.kr/images/common/S-LOGO.png"
        # products.append(product['cvs']+"에서 "+product['name']+"제품을 "+product['event']+"행사로 "+product['price']+"원에 판매")
        products.append(product)

    # 버튼 섹션
    button_actions = ActionsBlock(
        block_id=text,
        elements=[
            ButtonElement(
                text="전체", style="primary",
                action_id="전체", value=" "
            ),
            ButtonElement(
                text="1+1", style="danger",
                action_id="1+1", value="1"
            ),
            ButtonElement(
                text="2+1", style="danger",
                action_id="2+1", value="2"
            ),
            ButtonElement(
                text="3+1", style="danger",
                action_id="3+1", value="3"
            )
        ]
    )

    # 버튼 값에 따른 이벤트 정보 텍스트 값 설정
    if button == "1":
        text_event = "1+1"
    elif button == "2":
        text_event = "2+1"
    elif button == "3":
        text_event = "3+1"
    else:
        text_event = ""

    if len(products) == 0:
        result = []
        text3 = "*" + search_text.strip() + "*" + " 제품은 " + text_event +" 행사 중인 상품이 아닙니다." + ":sob:"

        result.append(SectionBlock(
            # text=search_text.strip() + " 제품은 행사 중인 상품이 아닙니다.",
            text = text3,
        ))
        return result + [button_actions]

    # 처음 섹션
    result = []
    text4 = "*" + search_text.strip() + "* " + text_event + " 행사 상품 조회 결과입니다." + ":sunglasses:"
    result.append(SectionBlock(
        text= text4,
    ))

    # 두 번째 섹션
    product_fields = []
    for product in products:
        image = ImageElement(
            image_url=product['image'],
            alt_text="제품 이미지"
        )
        text2 = "*" + product['name'] +" | " + product['cvs'] +"*"
        text2 += "\n" + product['price'] + "원 | " + product['event'] + ":tada:"
        product_fields.append(SectionBlock(
            text=text2,
            accessory=image
        ))

    return result + product_fields + [button_actions]

# 중복 답변 방지를 위한 함수
def sendMessage(text, channel):
    message = _crawl_cvs(text, "")
    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(message)
    )


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    Thread(target=sendMessage, args=(text, channel)).start()

@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리합니다
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))

    text = click_event.block_id
    button = click_event.value

    message_blocks = _crawl_cvs(text, button)

    # 메시지를 채널에 올립니다
    slack_web_client.chat_postMessage(
        channel=click_event.channel.id,
        blocks=extract_json(message_blocks)
    )

    # Slack에게 클릭 이벤트를 확인했다고 알려줍니다
    return "OK", 200

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('127.0.0.1', port=8080)
